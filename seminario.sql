-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2021 at 04:27 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seminario`
--

-- --------------------------------------------------------

--
-- Table structure for table `carrera`
--

CREATE TABLE `carrera` (
  `Codigo` varchar(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Representante` varchar(100) DEFAULT NULL,
  `Facultad` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carrera`
--

INSERT INTO `carrera` (`Codigo`, `Nombre`, `Representante`, `Facultad`) VALUES
('111', 'Ing Biotecnologica', 'Fernanda Pinto', 'Ingenieria'),
('112', 'Ing AgroIndustrial', 'Yeison Barbosa Solano', 'Ciencias Agrarias y del Ambiente'),
('115', 'Ing. En Sistemas', 'Camilo Alexander Jauregui Rojas', 'Ingenieria'),
('116', 'Ing. Civil', 'Omar Ramon', 'Ingenieria'),
('118', 'Ing. Biotecnologica', 'Camilo Alexander Jauregui Rojas', 'Ciencias Agrarias y del Ambiente'),
('135', 'Biologia', 'Juana Diaz Diaz', 'Ciencias Agrarias y del Ambiente'),
('168', 'Enfermeria', 'Camilo Alexander Jauregui Rojas', 'Ciencias de la Salud');

-- --------------------------------------------------------

--
-- Table structure for table `facultad`
--

CREATE TABLE `facultad` (
  `Nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facultad`
--

INSERT INTO `facultad` (`Nombre`) VALUES
('Ciencias Agrarias y del Ambiente'),
('Ciencias Basicas'),
('Ciencias de la Salud'),
('Ciencias Empresariales'),
('Educacion, Artes y Humanidades'),
('Ingenieria');

-- --------------------------------------------------------

--
-- Table structure for table `formulario`
--

CREATE TABLE `formulario` (
  `Codigo` int(30) NOT NULL,
  `Nombre` varchar(130) NOT NULL,
  `Ruta` varchar(150) NOT NULL,
  `Observacion` varchar(200) DEFAULT 'Sin Observacion',
  `Reclamo` varchar(100) DEFAULT 'Sin Reclamo',
  `Revision` int(11) DEFAULT 0,
  `Semillero` varchar(70) DEFAULT NULL,
  `Carrera` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formulario`
--

INSERT INTO `formulario` (`Codigo`, `Nombre`, `Ruta`, `Observacion`, `Reclamo`, `Revision`, `Semillero`, `Carrera`) VALUES
(43, 'Doc1 (1).pdf', 'http://localhost/img/Doc1 (1).pdf', 'Prueba 2', '', 1, 'Viral', NULL),
(65, 'Thu Nov 04 01:19:35 COT 2021Habilitacion.pdf', 'http://localhost/img/Habilitacion.pdf', NULL, NULL, 0, NULL, 'Ing. En Sistemas'),
(66, 'Tue Nov 30 10:58:37 COT 2021alfNota08.pdf', 'http://localhost/img/alfNota08.pdf', 'Mi trabajo', 'Sin reclamo', 1, 'Artes', NULL),
(70, 'Sun Dec 05 18:40:43 COT 2021Asociacion de Condensadores.docx', 'http://localhost/img/Asociacion de Condensadores.docx', NULL, NULL, 0, NULL, 'Ing. En Sistemas'),
(72, 'Mon Dec 06 20:49:34 COT 2021Asociacion de resistencias.pdf', 'http://localhost/img/Asociacion de resistencias.pdf', 'Aprobado', '', 1, 'Seguridad Informatica', NULL),
(74, 'Tue Dec 14 05:10:21 UTC 2021CV__CamiloJauregui (1).pdf', 'http://3.140.4.162/img/CV__CamiloJauregui (1).pdf', NULL, NULL, 0, NULL, 'Ing. En Sistemas'),
(75, 'Tue Dec 14 05:14:18 UTC 2021CotizaciÃ³nSoftware.pdf', 'http://3.140.4.162/img/CotizaciÃ³nSoftware.pdf', NULL, NULL, 0, 'Seguridad Informatica', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `imagen`
--

CREATE TABLE `imagen` (
  `Codigo` int(50) NOT NULL,
  `Observacion` longtext DEFAULT NULL,
  `Ruta` varchar(255) NOT NULL,
  `Semillero` varchar(70) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagen`
--

INSERT INTO `imagen` (`Codigo`, `Observacion`, `Ruta`, `Semillero`) VALUES
(166, '', 'http://3.140.4.162/img/1.jpg', 'Seguridad Informatica'),
(167, '', 'http://3.140.4.162/img/2.jpg', 'Seguridad Informatica'),
(168, '', 'http://3.140.4.162/img/3.jfif', 'Seguridad Informatica'),
(169, '', 'http://3.140.4.162/img/estudiantes de Cloud Computing en Colombia.jpg', 'Viral'),
(170, '', 'http://3.140.4.162/img/ufps.jpg', 'Seguridad Informatica');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `Codigo` varchar(11) NOT NULL,
  `Cedula` varchar(11) NOT NULL,
  `contraseña` varchar(15) NOT NULL,
  `Estado` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`Codigo`, `Cedula`, `contraseña`, `Estado`) VALUES
('1142020', '1', '1', 1),
('1151212', '1', '1', 1),
('1151325', '1', '1', 1),
('1151408', '1', '1', 1),
('1151700', '123', '1', 1),
('1687777', '1232', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `semillero`
--

CREATE TABLE `semillero` (
  `Codigo` varchar(20) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Representante` varchar(100) DEFAULT NULL,
  `Carrera` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `semillero`
--

INSERT INTO `semillero` (`Codigo`, `Nombre`, `Representante`, `Carrera`) VALUES
('1189', 'Linux Seguridad', 'Camilo Alexander Jauregui Rojas', 'Ing. En Sistemas'),
('123', 'Linux23', 'Omar Ramon', 'Biologia'),
('1515', 'Vive Lab', 'Fernanda Pinto', 'Ing. En Sistemas'),
('2', 'Seguridad Informatica', 'Omar Ramon', 'Ing. En Sistemas'),
('3', 'Viral', 'Omar Ramon', 'Ing. En Sistemas'),
('5', 'IOS', 'Fernanda Pinto', 'Ing. En Sistemas'),
('6', 'Artes', 'Fernanda Pinto', 'Ing. Civil'),
('7', 'CIMPAT', 'Juana Diaz Diaz', 'Ing Biotecnologica');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `Codigo` varchar(11) NOT NULL,
  `Cedula` varchar(30) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Carrera` varchar(100) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `Rol` varchar(20) NOT NULL,
  `foto` varchar(225) DEFAULT 'logo.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`Codigo`, `Cedula`, `Nombre`, `Correo`, `Carrera`, `telefono`, `Rol`, `foto`) VALUES
('1142020', '1', 'Amanda', '123@gmail.com', 'Ing Biotecnologica', '1', 'Director', 'vacio'),
('1151212', '1', 'Fernanda Pinto', 'fernandagarca@ufps.edu.co', 'Ing. En Sistemas', '111', 'Representante', 'http://3.140.4.162/img/pBzsn5bC_400x400.jpg'),
('1151325', '1111', 'Omar Ramon', 'Omarrm@ufps.edu.co', 'Ing. En Sistemas', '0001', 'Director', 'http://3.140.4.162/img/ufpslogo.jpg'),
('1151408', '1', 'Camilo Alexander Jauregui Rojas', 'camiloalexanderjr@ufps.edu.co', 'Ing. En Sistemas', '000000', 'Admin', 'http://3.140.4.162/img/ufpslogo.jpg'),
('1151612', '1090486586', 'Yeison Barbosa Solano', 'yeisonbs@ufps.edu.co', 'Ing. En Sistemas', '3202564864', 'Representante', 'vacio'),
('1151700', '123', 'Ramon', 'ramon@ufps.edu.co', 'Ing. Biotecnologica', '000', 'Admin', 'vacio'),
('1153108', '1010', 'Juana Diaz Diaz', 'juana@correo.com', 'Ing. Biotecnologica', '300122334', 'Director', 'vacio'),
('1687777', '1232', 'Petty Duaz', 'correo@gmail.com', 'Ing AgroIndustrial', '1234567', 'Representante', 'vacio');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Facultad` (`Facultad`),
  ADD KEY `Profesor` (`Representante`) USING BTREE,
  ADD KEY `Nombre` (`Nombre`);

--
-- Indexes for table `facultad`
--
ALTER TABLE `facultad`
  ADD PRIMARY KEY (`Nombre`) USING BTREE;

--
-- Indexes for table `formulario`
--
ALTER TABLE `formulario`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Semillero` (`Semillero`),
  ADD KEY `Carrera` (`Carrera`);

--
-- Indexes for table `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Semillero` (`Semillero`),
  ADD KEY `Semillero_2` (`Semillero`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`Codigo`);

--
-- Indexes for table `semillero`
--
ALTER TABLE `semillero`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Carrera` (`Carrera`),
  ADD KEY `Profesor` (`Representante`) USING BTREE,
  ADD KEY `Nombre` (`Nombre`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`Codigo`),
  ADD KEY `Codigo` (`Codigo`),
  ADD KEY `Nombre` (`Nombre`),
  ADD KEY `Carrera` (`Carrera`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `formulario`
--
ALTER TABLE `formulario`
  MODIFY `Codigo` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `imagen`
--
ALTER TABLE `imagen`
  MODIFY `Codigo` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carrera`
--
ALTER TABLE `carrera`
  ADD CONSTRAINT `carrera_ibfk_1` FOREIGN KEY (`Facultad`) REFERENCES `facultad` (`Nombre`),
  ADD CONSTRAINT `carrera_ibfk_2` FOREIGN KEY (`Representante`) REFERENCES `usuario` (`Nombre`) ON UPDATE CASCADE;

--
-- Constraints for table `formulario`
--
ALTER TABLE `formulario`
  ADD CONSTRAINT `formulario_ibfk_1` FOREIGN KEY (`Semillero`) REFERENCES `semillero` (`Nombre`),
  ADD CONSTRAINT `formulario_ibfk_2` FOREIGN KEY (`Carrera`) REFERENCES `carrera` (`Nombre`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `imagen`
--
ALTER TABLE `imagen`
  ADD CONSTRAINT `imagen_ibfk_1` FOREIGN KEY (`Semillero`) REFERENCES `semillero` (`Nombre`);

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`Codigo`) REFERENCES `usuario` (`Codigo`);

--
-- Constraints for table `semillero`
--
ALTER TABLE `semillero`
  ADD CONSTRAINT `semillero_ibfk_1` FOREIGN KEY (`Carrera`) REFERENCES `carrera` (`Nombre`),
  ADD CONSTRAINT `semillero_ibfk_2` FOREIGN KEY (`Representante`) REFERENCES `usuario` (`Nombre`) ON UPDATE CASCADE;

--
-- Constraints for table `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`Carrera`) REFERENCES `carrera` (`Nombre`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
