/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Login.LoginDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
public class CrearUsuario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String Codigo = request.getParameter("codigo");
            String Cedula = request.getParameter("cedula");
            String Contraseña = request.getParameter("contrasena");
            String msj = "";

            String accion = request.getParameter("accion");

            LoginDao l = new LoginDao();
            if (accion.equalsIgnoreCase("Crear")) {

                String e = request.getParameter("estado");
                int estado = 0;

                if (e.equalsIgnoreCase("Activo")) {
                    estado = 1;
                }
                boolean msj2 = l.crearCuenta(Codigo, Cedula, Contraseña, estado);
                if (msj2) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Tarea realizada con exito');");
                    out.println("location='Admin/MostrarCredenciales.jsp';");
                    out.println("</script>");
                    //response.sendRedirect("Admin/MostrarCredenciales.jsp");

                }else{
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Hubo un error, intentelo de nuevo');");
                    out.println("location='Admin/RegistrarCredenciales.jsp';");
                    out.println("</script>");
                }

            } else if (accion.equalsIgnoreCase("Hab")) {

                String Usuario = request.getParameter("id");

                if (l.HabilitarDeshabilitar(1, Usuario)) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Tarea realizada con exito');");
                    out.println("location='Admin/MostrarCredenciales.jsp';");
                    out.println("</script>");
                    //response.sendRedirect("Admin/MostrarCredenciales.jsp");

                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Hubo un error, intentelo de nuevo');");
                    out.println("location='Admin/MostrarCredenciales.jsp';");
                    out.println("</script>");
                    //response.sendRedirect("Admin/MostrarCredenciales.jsp");
                }
            } else if (accion.equalsIgnoreCase("DesH")) {

                String Usuario = request.getParameter("id");

                if (l.HabilitarDeshabilitar(0, Usuario)) {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Tarea realizada con exito');");
                    out.println("location='Admin/MostrarCredenciales.jsp';");
                    out.println("</script>");
                    //response.sendRedirect("Admin/MostrarCredenciales.jsp");

                } else {
                    out.println("<script type=\"text/javascript\">");
                    out.println("alert('Hubo un error, intentelo de nuevo');");
                    out.println("location='Admin/MostrarCredenciales.jsp';");
                    out.println("</script>");
                    //response.sendRedirect("Admin/MostrarCredenciales.jsp");
                }
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
