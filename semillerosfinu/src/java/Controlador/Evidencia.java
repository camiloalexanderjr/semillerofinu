/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Formulario.Formulario;
import Formulario.FormularioDao;
import Img.Img;
import Img.ImgDao;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author USUARIO
 */
public class Evidencia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static Date d;
    public int aux;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            String accion = request.getParameter("accion");

            Img img = new Img();
            ImgDao Im = new ImgDao();

            Formulario doc = new Formulario();
            FormularioDao docs = new FormularioDao();
            String obs = request.getParameter("textarea");
            d = new Date();

            switch (accion) {
                case "Imagenes":
                    ArrayList<String> lista = new ArrayList<>();
                    try {
                        FileItemFactory file = new DiskFileItemFactory();

                        ServletFileUpload fileUpload = new ServletFileUpload(file);

                        List items = fileUpload.parseRequest(request);

                        for (int i = 0; i < items.size(); i++) {

                            FileItem fileItem = (FileItem) items.get(i);
                            if (!fileItem.isFormField()) {
                                String hora = d.toString();
                                File f = new File("C:\\xampp\\htdocs\\img\\" + fileItem.getName());
                                fileItem.write(f);
                                img.setRuta("http://localhost/img/" + f.getName());
                                img.setSemillero(RutaIMGDOCS.semillero);
                            } else {
                                lista.add(fileItem.getString());
                            }
                        }
//                    p.setObs(lista.get(0));
                        if (Im.crearImg(img)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Imagen almacenada correctamente');");
                            out.println("location='Evidencias/Img.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Img.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Hubo un error, intentelo de nuevo');");
                            out.println("location='Evidencias/Img.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Img.jsp");
                        }
                    } catch (Exception e) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error');");
                        out.println("location='Evidencias/Img.jsp';");
                        out.println("</script>");
                    }
//                request.getRequestDispatcher("regFormulario?accion=Listar").forward(request, response);
                    break;
                case "Documentos":
                    ArrayList<String> listaDocs = new ArrayList<>();
                    try {
                        FileItemFactory file = new DiskFileItemFactory();

                        ServletFileUpload fileUpload = new ServletFileUpload(file);

                        List items = fileUpload.parseRequest(request);

                        for (int i = 0; i < items.size(); i++) {

                            FileItem fileItem = (FileItem) items.get(i);
                            if (!fileItem.isFormField()) {

                                File f = new File("c:\\xampp\\htdocs\\img\\" + fileItem.getName());
                                fileItem.write(f);
                                doc.setRuta("http://localhost/img/" + f.getName());
                                doc.setSemillero(RutaIMGDOCS.semillero);
                                doc.setNombre(d.toString() + fileItem.getName());

                            } else {
                                listaDocs.add(fileItem.getString());
                            }
                        }

                        System.out.println(doc.toString());
                        if (docs.crearFormulario(doc)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Documento almacenado correctamente');");
                            out.println("location='Evidencias/Formulario.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Formulario.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Hubo un error, intentelo de nuevo');");
                            out.println("location='Evidencias/Formulario.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Formulario.jsp");
                        }
                    } catch (Exception e) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error');");
                        out.println("location='Evidencias/Formulario.jsp';");
                        out.println("</script>");
                    }
//                request.getRequestDispatcher("regFormulario?accion=Listar").forward(request, response);
                    break;
                case "EliminarDoc":
                    try {
                        String nombre = request.getParameter("id");
                        if (docs.EliminarFormulario(nombre)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Documento eliminado correctamente');");
                            out.println("location='Evidencias/Formulario.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Formulario.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Hubo un error, intentelo de nuevo');");
                            out.println("location='Evidencias/Formulario.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Formulario.jsp");
                        }
                    } catch (Exception e) {
                        System.out.println("Error " + e);
                    }
                    break;
                case "RepreEliminarDoc":
                    try {
                        String nombre = request.getParameter("id");
                        if (docs.EliminarFormulario(nombre)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Documento eliminado correctamente');");
                            out.println("location='Representante/Documentos.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Representante/Documentos.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Hubo un error, intentelo de nuevo');");
                            out.println("location='Representante/Documentos.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Representante/Documentos.jsp");
                        }
                    } catch (Exception e) {
                        System.out.println("Error " + e);
                    }
                    break;
                case "DocumentosRepre":
                    ArrayList<String> listaDo = new ArrayList<>();
                    //String x = request.getParameter("observacion");
                    try {
                        FileItemFactory file = new DiskFileItemFactory();

                        ServletFileUpload fileUpload = new ServletFileUpload(file);

                        List items = fileUpload.parseRequest(request);

                        for (int i = 0; i < items.size(); i++) {

                            FileItem fileItem = (FileItem) items.get(i);
                            if (!fileItem.isFormField()) {

                                File f = new File("c:\\xampp\\htdocs\\img\\" + fileItem.getName());
                                fileItem.write(f);
                                doc.setRuta("http://localhost/img/" + f.getName());

                                doc.setNombre(d.toString() + fileItem.getName());

                                System.out.println("Nombre:" + doc.getNombre());
                                System.out.println("Observacion" + obs);
                                doc.setCarrera(Logins.carrera);
                                docs.actualizarObservacion(obs, doc.getNombre());
                            } else {
                                listaDo.add(fileItem.getString());
                            }
                        }

//                        System.out.println(doc.toString());
                        if (docs.crearFormulario(doc)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Documento guardado correctamente');");
                            out.println("location='Representante/Documentos.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Representante/Documentos.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Hubo un error');");
                            out.println("location='Representante/Documentos.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Representante/Documentos.jsp");
                        }
                    } catch (Exception e) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error');");
                        out.println("location='Representante/Documentos.jsp';");
                        out.println("</script>");
                    }
//                request.getRequestDispatcher("regFormulario?accion=Listar").forward(request, response);
                    break;
                case "EliminarDocRepre":
                    try {
                        String nombre = request.getParameter("id");
                        if (docs.EliminarFormulario(nombre)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Documento eliminado correctamente');");
                            out.println("location='Evidencias/Formulario.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Formulario.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Hubo un error');");
                            out.println("location='Evidencias/Formulario.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Formulario.jsp");
                        }
                    } catch (Exception e) {
                        System.out.println("Error " + e);
                    }
                    break;
                case "EliminarImg":
                    try {
                        String cod = request.getParameter("id");
                        if (Im.EliminarImg(cod)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Imagen eliminada correctamente');");
                            out.println("location='Evidencias/Img.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Img.jsp");
                        } else {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Documento eliminado correctamente');");
                            out.println("location='Evidencias/Img.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Evidencias/Img.jsp");
                        }
                    } catch (Exception e) {
                        System.out.println("Error " + e);
                    }
                    break;
                case "Enviar":
                    try {
                        String cod = request.getParameter("nombre");
                        String obse = request.getParameter("observacion");
                        String revisado = request.getParameter("revisado");
                        Integer revi = 0;
                        if (revisado != null) {
                            revi = Integer.parseInt(revisado);
                            //aux = revi;
                            System.out.println("Valor aux: " + aux);
                            //doc.setRevision(revi);
                        }
                        if (obse.equals("")) {
                            obse = "Sin observacion";
                        }
                        System.out.println("Observacion: " + obse);
                        System.out.println("codigo: " + cod);
                        System.out.println("revisado: " + revi);
                        docs.actualizar(obse, cod, revi);
                        docs.actualizarReclamo("Sin reclamo", cod);
                        response.sendRedirect("VerEvidencias/Formulario.jsp");
                    } catch (Exception e) {
                        System.out.println("Error " + e);
                    }
                    break;
                case "EnviarRec":
                    System.out.println("entra---------------");
                    String reclamo = request.getParameter("reclamos");
                    String cod = request.getParameter("id");
                    
                    System.out.println("Codigo: " + cod);
                    System.out.println("Reclamo: " + reclamo);
                    //docs.actualizar("Hay reclamo nuevo", cod, Integer.parseInt(revisado));
                    docs.actualizarReclamo(reclamo, cod);
                    response.sendRedirect("Evidencias/Formulario.jsp");
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
