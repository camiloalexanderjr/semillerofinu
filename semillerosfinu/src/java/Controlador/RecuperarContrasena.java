/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Listas.UsuarioLista;
import Login.LoginDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author USUARIO
 */
public class RecuperarContrasena extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String accion = request.getParameter("accion");

            LoginDao l = new LoginDao();
            UsuarioLista u = new UsuarioLista();

            String destinatario = request.getParameter("correo");

            switch (accion) {
                case "Enviar":
                    if (u.buscarCorreo(destinatario) == true) {
                        try {
                            Properties propiedad = new Properties();
                            propiedad.put("mail.smtp.host", "smtp.gmail.com");
                            propiedad.put("mail.smtp.port", "587");
                            propiedad.put("mail.smtp.auth", "true");
                            propiedad.put("mail.smtp.starttls.enable", "true");
                            propiedad.put("mail.smtp.ssl.trust", "smtp.gmail.com");
                            propiedad.put("mail.smtp.ssl.protocols", "TLSv1.2");

                            String CorreoEnvia = "ufps.centros.recreativos@gmail.com";
                            String contrasena = "lgnbafbsitnovmol";

                            Authenticator auth = new Authenticator() {
                                @Override
                                public PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(CorreoEnvia, contrasena);
                                }
                            };

                            String Asunto = "Recovery Password";
                            String msj = "http://3.144.5.86:8084/Recover/CambiarPass.jsp";

                            Session session = Session.getInstance(propiedad, auth);
                            System.out.println("----------------------------");

                            // creates a new e-mail message
                            Message mail = new MimeMessage(session);
                            try {
                                mail.setFrom(new InternetAddress(CorreoEnvia));
                                InternetAddress[] toAddresses = {new InternetAddress(destinatario), new InternetAddress("omarmontes.879@gmail.com")};

                                mail.setRecipients(Message.RecipientType.TO, toAddresses);
                                mail.setSubject(Asunto);
                                mail.setText(msj);
                                mail.setSentDate(new Date());

//                                transporte.connect(CorreoEnvia, contrasena);
//                                transporte.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
//                                transporte.close();
                                Transport.send(mail);

                                System.out.println("Enviado--------------------");
                                out.println("<script type=\"text/javascript\">");
                                out.println("alert('Mensaje enviado con exito');");
                                out.println("location='Login/indexLogin.html';");
                                out.println("</script>");
                                //response.sendRedirect("Login/indexLogin.html");

                            } catch (AddressException ex) {
                                Logger.getLogger(RecuperarContrasena.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (MessagingException ex) {
                                Logger.getLogger(RecuperarContrasena.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (Exception e) {
                            System.out.println("Error" + e);
                        }
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Correo no encontrado o error de conexion');");
                        out.println("location='Recover/RecoveryPass.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Recover/RecoveryPass.jsp");
                        System.out.println("Correo no encontrado");
                    }
                    break;
                case "Actualizar":

                    String pass = request.getParameter("pass");
                    if (l.actualizar(pass, UsuarioLista.usuario)) {
                        try {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Tarea realizada con exito');");
                            out.println("location='Login/indexLogin.html';");
                            out.println("</script>");
                            //response.sendRedirect("Login/indexLogin.html");
                        } catch (Exception e) {
                            System.out.println("Error--------" + e);
                        }
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Recover/RecoveryPass.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Recover/RecoveryPass.jsp");
                    }
                    break;
                default:

                    throw new AssertionError();

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
