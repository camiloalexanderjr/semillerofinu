/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import static Controlador.RutaIMGDOCS.semillero;
import Semillero.SemilleroDao;
import Usuario.Usuario;
import Usuario.UsuarioDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author YorgenGalvis
 */
public class DirectorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static String semillero = "";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String accion = request.getParameter("action");
            semillero = request.getParameter("id");

            if (accion.equalsIgnoreCase("editRepre")) {
                response.sendRedirect("Admin/editRepre.jsp");
            } else if (accion.equalsIgnoreCase("updateRepre")) {
                String codRepresentante = request.getParameter("representante");
                String name=getNombreUser(codRepresentante);
                String codigo=request.getParameter("id");
                System.out.println("CODIGO "+codigo);
                SemilleroDao sdo=new SemilleroDao();
                sdo.updateRepresentate(codigo,"Representante",name);
                semillero=codigo;
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Tarea realizada con exito');");
                out.println("location='Admin/mostrarSemillero.jsp';");
                out.println("</script>");
                //response.sendRedirect("Admin/mostrarSemillero.jsp");
            }

        }
    }

    private String getNombreUser(String codRepresentante) {
        UsuarioDao r = new UsuarioDao();
        List<Usuario> rep = r.llenarLista();
        String nombre = null;
        for (Usuario u : rep) {
            if (u.getCodigo().equals(codRepresentante)) {
                return u.getNombre();
            }
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
