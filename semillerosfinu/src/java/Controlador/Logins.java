/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Listas.UsuarioLista;
import Login.LoginDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author USUARIO
 */
public class Logins extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static String user = "";
    public static String carrera = "";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            String Codigo = request.getParameter("codigo");
            String Cedula = request.getParameter("cedula");
            String Contraseña = request.getParameter("contrasena");

            LoginDao l = new LoginDao();

            UsuarioLista x = new UsuarioLista();

            try {
                boolean aux = l.validar(Codigo, Cedula, Contraseña);
                user = Codigo;

                String msj = x.buscar(Codigo).getRol();
                carrera = x.buscar(Codigo).getCarrera();
                if (aux) {
                    if (msj.equalsIgnoreCase("Admin")) {
                        HttpSession sesion = request.getSession();

                        sesion.setAttribute("registro", "Creado");
                        response.sendRedirect("Admin/VistaP.jsp");
                        request.getRequestDispatcher("Admin/VistaP.jsp").forward(request, response);

                    } else if (msj.equalsIgnoreCase("Director")) {

                        HttpSession sesion = request.getSession();

                        sesion.setAttribute("registro", "Creado");
                        response.sendRedirect("Director/VistaP.jsp");
                    } else if (msj.equalsIgnoreCase("Representante")) {

                        HttpSession sesion = request.getSession();

                        sesion.setAttribute("registro", "Creado");
                        response.sendRedirect("Representante/VistaP.jsp");
                    } else {
                        response.sendRedirect("Login/indexLogin.html");

                    }
                } else {  
                          out.println("<script type=\"text/javascript\">");
                          out.println("alert('Usuario o contraseña erroneos');");
                          out.println("location='Login/indexLogin.html';");
                          out.println("</script>");
                          //response.sendRedirect("Login/indexLogin.html");
                }
            } catch (Exception e) {
                out.println("<script type=\"text/javascript\">");
                out.println("alert('Usuario o contraseña erroneos');");
                out.println("location='Login/indexLogin.html';");
                out.println("</script>");
                System.out.println("error1----------------" + e);
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
