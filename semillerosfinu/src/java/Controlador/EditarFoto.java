/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Usuario.UsuarioDao;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author USUARIO
 */
public class EditarFoto extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            String Foto = "";
            boolean p = false;

            UsuarioDao user = new UsuarioDao();

            String accion = request.getParameter("accion");

            ArrayList<String> lista = new ArrayList<>();
            try {
                FileItemFactory file = new DiskFileItemFactory();
                System.out.println("----------1");
                ServletFileUpload fileUpload = new ServletFileUpload(file);
                System.out.println("---------------2");
                List items = fileUpload.parseRequest(request);
                System.out.println("--------------3");
                for (int i = 0; i < items.size(); i++) {
                    System.out.println("------------4");

                    FileItem fileItem = (FileItem) items.get(i);
                    if (!fileItem.isFormField()) {
                        File f = new File("C:\\xampp\\htdocs\\img\\" + fileItem.getName());
                        fileItem.write(f);
                        Foto = ("http://localhost/img/" + f.getName());
                        System.out.println("entra bien");
                        p = true;
                    } else {
                        lista.add(fileItem.getString());
                    }
                }
            } catch (Exception e) {
                System.out.println(e);
                System.out.println("foto vacia");
            }

            switch (accion) {
                case "editarAdmin":
                    if (user.actualizarFoto(Logins.user, Foto) == true) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Imagen almacenada correctamente');");
                        out.println("location='Admin/Datos.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Evidencias/Img.jsp");
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Admin/Datos.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Evidencias/Img.jsp");
                    }
                    break;
                case "editarRepre":
                    if (user.actualizarFoto(Logins.user, Foto) == true) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Imagen almacenada correctamente');");
                        out.println("location='Representante/Datos.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Evidencias/Img.jsp");
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Representante/Datos.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Evidencias/Img.jsp");
                    }
                    break;

                case "editarDirec":
                    if (user.actualizarFoto(Logins.user, Foto) == true) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Imagen almacenada correctamente');");
                        out.println("location='Director/Datos.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Evidencias/Img.jsp");
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Director/Datos.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Evidencias/Img.jsp");
                    }
                    break;
                default:
                    throw new AssertionError();

            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
