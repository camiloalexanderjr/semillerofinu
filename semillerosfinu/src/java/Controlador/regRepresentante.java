/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Usuario.UsuarioDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author USUARIO
 */
public class regRepresentante extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            String accion = request.getParameter("accion");
            System.out.println("accion---" + accion);
            String codigo = request.getParameter("codigo");
            String nombre = request.getParameter("nombre");
            String correo = request.getParameter("correo");
            String cedula = request.getParameter("cedula");
            String carrera = request.getParameter("carrera");
            String telefono = request.getParameter("telefono");
            String rol = request.getParameter("rol");
            String foto="http://localhost/img/logo.png";
            boolean msj = false;
            UsuarioDao r = new UsuarioDao();
            switch (accion) {
                case "crear":
                    try {
                        msj = r.crearRepresentante(codigo, cedula, nombre, correo, carrera, telefono, rol,foto);
                    } catch (Exception e) {
                        System.out.println("error----------------" + e);
                    }

                    if (msj) {
                        HttpSession sesion = request.getSession();
                        sesion.setAttribute("registro", "Creado");
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Tarea realizada con exito');");
                        out.println("location='Admin/VistaP.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("Admin/VistaP.jsp");
                    } else {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Admin/VistaP.jsp';");
                        out.println("</script>");
                        //response.sendRedirect("index.html");
                    }
                    break;
                case "editarAdmin":
                    try {
                         if (r.actualizar(nombre, correo, telefono, codigo)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Tarea realizada con exito');");
                            out.println("location='Admin/Datos.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Admin/Datos.jsp");
                        }
                    } catch (Exception e) {
                        System.out.println("Error 1"+e);
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Admin/Datos.jsp';");
                        out.println("</script>");
                    }
                    break;
                case "editarRepre":
                    try {
                        if (r.actualizar(nombre, correo, telefono, codigo)) {
                            out.println("<script type=\"text/javascript\">");
                            out.println("alert('Tarea realizada con exito');");
                            out.println("location='Representante/Datos.jsp';");
                            out.println("</script>");
                            //response.sendRedirect("Representante/Datos.jsp");
                        }
                    } catch (Exception e) {
                        System.out.println("Error 1"+e);
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Hubo un error, intentelo de nuevo');");
                        out.println("location='Representante/Datos.jsp';");
                        out.println("</script>");
                    }
                    break;
                case "editarDirector":
                       if(r.actualizar(nombre, correo, telefono, codigo)){
                           out.println("<script type=\"text/javascript\">");
                           out.println("alert('Tarea realizada con exito');");
                           out.println("location='Director/Datos.jsp';");
                           out.println("</script>");
                           //response.sendRedirect("Director/Datos.jsp");
                        }
                    break;
                default:
                    throw new AssertionError();
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
