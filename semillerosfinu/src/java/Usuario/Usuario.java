/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

/**
 *
 * @author USUARIO
 */
public class Usuario {

    String codigo;
    String cedula;
    String nombre;
    String correo;
    String carrera;
    String telefono;
    String rol;
    String foto;
    public Usuario() {
    }

    public Usuario(String codigo, String cedula, String nombre, String correo, String carrera, String telefono, String rol) {
        this.codigo = codigo;
        this.cedula = cedula;
        this.nombre = nombre;
        this.correo = correo;
        this.carrera = carrera;
        this.telefono = telefono;
        this.rol = rol;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Usuario{" + "codigo=" + codigo + ", cedula=" + cedula + ", nombre=" + nombre + ", correo=" + correo + ", carrera=" + carrera + ", telefono=" + telefono + ", rol=" + rol + '}';
    }

  
}
