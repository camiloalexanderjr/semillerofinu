/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class UsuarioDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public boolean crearRepresentante(String codigo, String cedula, String nombre, String correo, String carrera, String telefono, String rol, String foto) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("INSERT INTO usuario(codigo, cedula, nombre, correo, carrera, telefono, rol, foto) VALUES (?,?,?,?,?,?,?,?)");

            pps.setString(1, codigo);
            pps.setString(2, cedula);
            pps.setString(3, nombre);
            pps.setString(4, correo);
            pps.setString(5, carrera);
            pps.setString(6, telefono);
            pps.setString(7, rol);
            pps.setString(8, foto);
            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println(ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }
    
    

    public boolean actualizar(String nombre, String correo, String telefono, String user) {
        try {
            pps = con.Conexion().prepareStatement("UPDATE `usuario` SET `Nombre`=?,`Correo`=?,`telefono`=? WHERE `Codigo`=?");
            pps.setString(1, nombre);
            pps.setString(2, correo);
            pps.setString(3, telefono);
            pps.setString(4, user);
            pps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("error a" + e);
        }
        return false;
    }
    public boolean actualizarFoto(String codigo,String Foto) {
        try {
            pps = con.Conexion().prepareStatement("UPDATE `usuario` SET `foto`=? WHERE `Codigo`=?");
            pps.setString(1, Foto);
            pps.setString(2, codigo);
            
            pps.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println("error a" + e);
        }
        return false;
    }

    public List llenarLista() {
        List<Usuario> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM usuario");
            rs = pps.executeQuery();
            while (rs.next()) {
                Usuario a = new Usuario();

                a.setCodigo(rs.getString(1));
                a.setCedula(rs.getString(2));
                a.setNombre(rs.getString(3));
                a.setCorreo(rs.getString(4));
                a.setCarrera(rs.getString(5));
                a.setTelefono(rs.getString(6));
                a.setRol(rs.getString(7));
                a.setFoto(rs.getString(8));
                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error " + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

//    public static void main(String[] args) {
//        UsuarioDao x = new UsuarioDao();
//
//        
//        System.out.println( x.actualizar("Camilo Rojas", "camiloalexander134@gmail.com", "1", "1151408"));
////        for (int i = 0; i < x.llenarLista().size(); i++) {
////            System.out.println(x.llenarLista().get(i).toString());
////        }
//
////        x.crearRepresentante("1101","111", "Yeison Barbosa","camiloalexanderjr@ufps.edu.co", "Sistemas", "212312", "admin");
//    }
}
