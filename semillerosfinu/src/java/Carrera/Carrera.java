/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Carrera;

/**
 *
 * @author USUARIO
 */
public class Carrera {

    String codigo;
    String nombre;
    String repre;
    String facultad;

    public Carrera() {
    }

    public Carrera(String codigo, String nombre, String repre, String facultad) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.repre = repre;
        this.facultad = facultad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRepre() {
        return repre;
    }

    public void setRepre(String repre) {
        this.repre = repre;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }

    @Override
    public String toString() {
        return "Carrera{" + "codigo=" + codigo + ", nombre=" + nombre + ", repre=" + repre + ", facultad=" + facultad + '}';
    }
 
    
}
