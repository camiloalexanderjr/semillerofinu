/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Carrera;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class CarreraDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public boolean crearCarrera(String Codigo, String Nombre, String repre, String facultad) {
     
        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("INSERT INTO carrera(Codigo,Nombre, Representante, Facultad) VALUES (?,?,?,?)");
           
            pps.setString(1, Codigo);
            pps.setString(2, Nombre);
            pps.setString(3, repre);
            pps.setString(4, facultad);
            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println("error 1 "+ex);
            msj= false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }
    
    public List llenarLista() {
        List<Carrera> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM carrera");
            rs = pps.executeQuery();
            while (rs.next()) {
                Carrera a = new Carrera();

                a.setCodigo(rs.getString(1));
                a.setNombre(rs.getString(2));
                a.setRepre(rs.getString(3));
                a.setFacultad(rs.getString(4));
                
                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error 2 " + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

       public boolean actualizar(String repre, String codigo) {
        boolean msj=false;
        try {
            pps = con.Conexion().prepareStatement("Update carrera set representante='" + repre + "'WHERE Codigo=?");
            pps.setString(1, codigo);
            pps.executeUpdate();
            msj=true;
        } catch (Exception e) {
            System.out.println("error" + e);
        }
        return msj;
    }
    
    
    
     public static void main(String[] args) {
       CarreraDao x = new CarreraDao();
//     
//         for (int i = 0; i < x.llenarLista().size(); i++) {
//             System.out.println(  x.llenarLista().get(i).toString());
//         }

//        x.crearFacultad("Artes", "123123","Camilo Jauregui");
    }
}


