/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Img;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class ImgDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public boolean crearImg(Img f) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("INSERT INTO imagen(Codigo,Observacion, Ruta,Semillero) VALUES (?,?,?,?)");

            pps.setInt(1, 0);
            pps.setString(2, "");
            pps.setString(3, f.getRuta());
            pps.setString(4, f.getSemillero());
            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println("Error img 1" + ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        System.out.println(msj);
        return msj;
    }

    public boolean EliminarImg(String codigo) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("Delete From Imagen WHERE codigo='" + codigo + "'");

            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println("error formu 1" + ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }

    public List llenarLista() {
        List<Img> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM imagen");
            rs = pps.executeQuery();
            while (rs.next()) {
                Img a = new Img();

                a.setCodigo(rs.getInt(1));
                a.setObservacion(rs.getString(2));
                a.setRuta(rs.getString(3));
                a.setSemillero(rs.getString(4));

                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error img 2 " + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

    public static void main(String[] args) {
        ImgDao x = new ImgDao();
//
        for (int i = 0; i < x.llenarLista().size(); i++) {
            System.out.println(x.llenarLista().get(i).toString());
        }
//         System.out.println( x.crearFormulario("3", "Camilo Jauregui"));

    }
}
