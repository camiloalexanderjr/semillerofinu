/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Img;

/**
 *
 * @author USUARIO
 */
public class Img {
    int codigo;
    String Observacion;
    String Ruta;
    String Semillero;

    public Img() {
    }

    public Img(int codigo, String Observacion, String Ruta, String Semillero) {
        this.codigo = codigo;
        this.Observacion = Observacion;
        this.Ruta = Ruta;
        this.Semillero = Semillero;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }

    public String getRuta() {
        return Ruta;
    }

    public void setRuta(String Ruta) {
        this.Ruta = Ruta;
    }

    public String getSemillero() {
        return Semillero;
    }

    public void setSemillero(String Semillero) {
        this.Semillero = Semillero;
    }

    @Override
    public String toString() {
        return "Img{" + "codigo=" + codigo + ", Observacion=" + Observacion + ", Ruta=" + Ruta + ", Semillero=" + Semillero + '}';
    }
    
}
