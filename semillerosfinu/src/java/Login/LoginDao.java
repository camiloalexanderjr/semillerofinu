/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class LoginDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public boolean crearCuenta(String codigo, String cedula, String contraseña, int estado) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("INSERT INTO login(Codigo,Cedula, Contraseña, estado) VALUES (?,?,?,?)");

            pps.setString(1, codigo);
            pps.setString(2, cedula);
            pps.setString(3, contraseña);
            pps.setInt(4, estado);
            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println(ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }

    public List llenarLista() {
        List<Login> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM login");
            rs = pps.executeQuery();
            while (rs.next()) {
                Login a = new Login();

                a.setCodigo(rs.getString(1));
                a.setCedula(rs.getString(2));
                a.setContrasena(rs.getString(3));
                a.setEstado(rs.getInt(4));

                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error " + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

    public boolean validar(String codigo, String cedula, String contraseña) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM login WHERE Codigo=? and Cedula=? and contraseña=? and estado='1' ");
            //            String sql = "SELECT * FROM login(Codigo,Cedula, Contraseña) VALUES  (?,?,?)";
            pps.setString(1, codigo);
            pps.setString(2, cedula);
            pps.setString(3, contraseña);
            rs = pps.executeQuery();

            while (rs.next()) {

                msj = true;
            }
        } catch (Exception e) {
            System.out.println("Error " + e);

        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
       return msj;
    }

    public boolean actualizar(String pass, String user) {
        boolean msj=false;
        try {
            pps = con.Conexion().prepareStatement("Update login set contraseña='" + pass + "'WHERE Codigo=?");
            pps.setString(1, user);
            pps.executeUpdate();
            msj=true;
        } catch (Exception e) {
            System.out.println("error" + e);
        }
        return msj;
    }
    public boolean HabilitarDeshabilitar(int estado, String user) {
        boolean msj=false;
        try {
            pps = con.Conexion().prepareStatement("Update login set estado='" + estado + "'WHERE Codigo=?");
            pps.setString(1, user);
            pps.executeUpdate();
            msj=true;
        } catch (Exception e) {
            System.out.println("error" + e);
        }
        return msj;
    }

    public static void main(String[] args) {
        LoginDao a = new LoginDao();

        System.out.println(a.validar("09", "09", "09"));

    }

}
