/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

/**
 *
 * @author USUARIO
 */
public class Login {
    
    
    String codigo;
    String cedula;
    String contrasena;
    int estado;

    public Login(String codigo, String cedula, String contrasena, int estado) {
        this.codigo = codigo;
        this.cedula = cedula;
        this.contrasena = contrasena;
        this.estado = estado;
    }

  

    public Login() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    
    
    @Override
    public String toString() {
        return "LoginDao{" + "codigo=" + codigo + ", cedula=" + cedula + ", contrasena=" + contrasena + '}';
    }
    
    
    
    
}
