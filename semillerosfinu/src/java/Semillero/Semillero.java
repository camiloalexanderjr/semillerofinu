/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semillero;

/**
 *
 * @author USUARIO
 */
public class Semillero {

    String codigo;
    String nombre;
    String datos;
    String representante;
    String carrera;

    public Semillero() {
    } 

    public Semillero(String codigo, String nombre, String datos, String representante, String carrera) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.datos = datos;
        this.representante = representante;
        this.carrera = carrera;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setFacultad(String facultad) {
        this.carrera = facultad;
    }

    @Override
    public String toString() {
        return "Semillero{" + "codigo=" + codigo + ", nombre=" + nombre + ", datos=" + datos + ", representante=" + representante + ", facultad=" + carrera + '}';
    }

}
