/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Semillero;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class SemilleroDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public boolean crearSemillero(String codigo, String nombre, String Representante, String Carrera) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("INSERT INTO semillero(Codigo,Nombre, Representante, Carrera) VALUES (?,?,?,?)");

            pps.setString(1, codigo);
            pps.setString(2, nombre);
            pps.setString(3, Representante);
            pps.setString(4, Carrera);
            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println("error semillero 1 " + ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }

    public Semillero getId(String id) {
        List<Semillero> lista = this.llenarLista();
        for (Semillero se : lista) {
            if (se.codigo.equals(id)) {
                return se;
            }
        }
        return null;
    }

    public void updateRepresentate(String codSemillero, String campo, String valor) {
        try {
            String sql = "UPDATE semillero SET "+campo+" = ? WHERE Codigo = ?";
            pps = con.Conexion().prepareStatement(sql);
            pps.setString(1, valor);
            pps.setString(2, codSemillero);
            pps.execute();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List llenarLista() {
        List<Semillero> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM semillero");
            rs = pps.executeQuery();
            while (rs.next()) {
                Semillero a = new Semillero();

                a.setCodigo(rs.getString(1));
                a.setNombre(rs.getString(2));
                a.setRepresentante(rs.getString(3));
                a.setFacultad(rs.getString(4));

                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error semillero 2 " + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

    public static void main(String[] args) {
        SemilleroDao x = new SemilleroDao();

        for (int i = 0; i < x.llenarLista().size(); i++) {
            System.out.println(x.llenarLista().get(i).toString());
        }
    }
}
