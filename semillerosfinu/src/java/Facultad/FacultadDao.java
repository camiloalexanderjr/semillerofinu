/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Facultad;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class FacultadDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public List llenarLista() {
        List<Facultad> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM facultad");
            rs = pps.executeQuery();
            while (rs.next()) {
                Facultad a = new Facultad();

                a.setNombre(rs.getString(1));
                
                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error " + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

    
     public static void main(String[] args) {
       FacultadDao x = new FacultadDao();
     
         for (int i = 0; i < x.llenarLista().size(); i++) {
             System.out.println(  x.llenarLista().get(i).toString());
         }

    }
}


