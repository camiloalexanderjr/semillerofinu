/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formulario;

/**
 *
 * @author USUARIO
 */
public class Formulario {

    String codigo;
    String Nombre;
    String ruta;
    String Observacion;
    String Reclamo;
    int Revision;
    String Semillero;
    String Carrera;

    public Formulario() {
    }

    public Formulario(String codigo, String Nombre, String ruta, String Observacion, String Reclamo, int Revision, String Semillero, String Carrera) {
        this.codigo = codigo;
        this.Nombre = Nombre;
        this.ruta = ruta;
        this.Observacion = Observacion;
        this.Reclamo = Reclamo;
        this.Revision = Revision;
        this.Semillero = Semillero;
        this.Carrera = Carrera;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }

    public String getReclamo() {
        return Reclamo;
    }

    public void setReclamo(String Reclamo) {
        this.Reclamo = Reclamo;
    }

    public int getRevision() {
        return Revision;
    }

    public void setRevision(int Revision) {
        this.Revision = Revision;
    }

    public String getSemillero() {
        return Semillero;
    }

    public void setSemillero(String Semillero) {
        this.Semillero = Semillero;
    }

    public String getCarrera() {
        return Carrera;
    }

    public void setCarrera(String Carrera) {
        this.Carrera = Carrera;
    }

    @Override
    public String toString() {
        return "Formulario{" + "codigo=" + codigo + ", Nombre=" + Nombre + ", ruta=" + ruta + ", Observacion=" + Observacion + ", Reclamo=" + Reclamo + ", Revision=" + Revision + ", Semillero=" + Semillero + ", Carrera=" + Carrera + '}';
    }
    
    
    

}