/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formulario;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class FormularioDao {

    Conexion con = new Conexion();
    Connection cn = con.Conexion();
    PreparedStatement pps = null;
    ResultSet rs = null;

    public boolean crearFormulario(Formulario f) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("INSERT INTO formulario(Codigo,Nombre, Ruta,observacion,Reclamo,Revision,Semillero,Carrera) VALUES (?,?,?,?,?,?,?,?)");

            pps.setInt(1, 0);
            pps.setString(2, f.getNombre());
            pps.setString(3, f.getRuta());
            pps.setString(4, f.getObservacion());
            pps.setString(5, f.getReclamo());
            pps.setInt(6, f.getRevision());
            pps.setString(7, f.getSemillero());
            pps.setString(8, f.getCarrera());
            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println("error formu 1" + ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }

    public boolean EliminarFormulario(String Nombre) {

        boolean msj = false;
        try {
            pps = con.Conexion().prepareStatement("Delete From formulario WHERE Nombre='" + Nombre + "'");

            pps.executeUpdate();
            msj = true;

        } catch (Exception ex) {
            System.out.println("error formu 1" + ex);
            msj = false;
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return msj;
    }

    public List llenarLista() {
        List<Formulario> lista = new ArrayList<>();
        try {
            pps = con.Conexion().prepareStatement("SELECT * FROM formulario");
            rs = pps.executeQuery();
            while (rs.next()) {
                Formulario a = new Formulario();

                a.setCodigo(rs.getString(1));
                a.setNombre(rs.getString(2));
                a.setRuta(rs.getString(3));
                a.setObservacion(rs.getString(4));
                a.setReclamo(rs.getString(5));
                a.setRevision(rs.getInt(6));
                a.setSemillero(rs.getString(7));
                a.setCarrera(rs.getString(8));

                lista.add(a);
            }
        } catch (Exception e) {
            System.out.println("Error formu 2" + e);
        } finally {
            try {
                con.Desconectar();
            } catch (Exception e) {

            }
        }
        return lista;
    }

    public void actualizar(String obs, String nombre,Integer revi) {
        
        try {
            pps = con.Conexion().prepareStatement("Update formulario set observacion='" + obs + "', Revision='"+revi+"' WHERE Nombre=?");
            pps.setString(1, nombre);
            pps.executeUpdate();
           
        } catch (Exception e) {
            System.out.println("error" + e);
        }
    }
    
    public void actualizarObservacion(String obs, String nombre) {
        
        try {
            pps = con.Conexion().prepareStatement("Update formulario set observacion='" + obs + "'WHERE Nombre=?");
            pps.setString(1, nombre);
            pps.executeUpdate();
           
        } catch (Exception e) {
            System.out.println("error" + e);
        }
    }
    
    public void actualizarReclamo(String reclamo, String cod) {
        
        try {
            pps = con.Conexion().prepareStatement("Update formulario set Reclamo='" + reclamo + "' WHERE codigo=?");
            pps.setString(1, cod);
            pps.executeUpdate();
           
        } catch (Exception e) {
            System.out.println("error" + e);
        }
    }

    public static void main(String[] args) {
        FormularioDao x = new FormularioDao();
//
//        for (int i = 0; i < x.llenarLista().size(); i++) {
//            System.out.println(x.llenarLista().get(i).toString());
//        }
//System.out.println(x.actualizar("asdasdasdads", "Wed Jun 16 13:24:38 COT 2021CI-MUS-02 Manual del Usuario.docx"));


//        x.EliminarFormulario("Documentos");
//         System.out.println( x.crearFormulario(1,"Camilo Jauregui","123","","",1,"Linux"));

    }

}
