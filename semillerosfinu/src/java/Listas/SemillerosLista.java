/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

import Semillero.Semillero;
import Semillero.SemilleroDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class SemillerosLista {

    SemilleroDao s = new SemilleroDao();

    public Semillero buscar(String nombre) {
        Semillero n = null;

        List<Semillero> lista = s.llenarLista();
        for (Semillero sem : lista) {
            if (sem.getNombre().equalsIgnoreCase(nombre)) {
                n = sem;
            }
        }
        return n;
    }
    UsuarioLista u = new UsuarioLista();

    public List buscarPorUser(String codigo) {
         List<Semillero> listS = new ArrayList<>();
       
        Semillero n = null;

        List<Semillero> lista = s.llenarLista();
        for (Semillero sem : lista) {

            if (sem.getRepresentante().equalsIgnoreCase(u.buscar(codigo).getNombre())) {
                listS.add(sem);
            }
        }
        return listS;
    }
    public List buscarPorCarrera(String carrera) {
         List<Semillero> listS = new ArrayList<>();
       
        Semillero n = null;

        List<Semillero> lista = s.llenarLista();
        for (Semillero sem : lista) { 

            if (sem.getCarrera().equalsIgnoreCase(carrera)) {
                listS.add(sem);
            }
        }
        return listS;
    }

    public static void main(String[] args) {
        SemillerosLista s = new SemillerosLista();
       for (int i = 0; i < s.buscarPorUser("1151408").size(); i++) {
        
        
             System.out.println( s.buscarPorUser("1151408").get(i).toString());
         }
//        System.out.println(s.buscarPorUser("1151408"));
    }

}
