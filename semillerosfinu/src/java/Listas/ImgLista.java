/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

import Img.Img;
import Img.ImgDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class ImgLista {

    ImgDao img = new ImgDao();

    public Img buscar(String nombre) {
        Img n = null;

        List<Img> lista = img.llenarLista();
        for (Img imgs : lista) {
            if (imgs.getRuta().equalsIgnoreCase(nombre)) {
                n = imgs;
            }
        }
        return n;
    }
    SemillerosLista u = new SemillerosLista();

    public List buscarPorSemillero(String Nombre) {
        List<Img> listS = new ArrayList<>();

        List<Img> lista = img.llenarLista();
        for (Img imgs : lista) {
            if (imgs.getSemillero() != null) {
                if (imgs.getSemillero().equalsIgnoreCase(Nombre)) {
                    listS.add(imgs);
                }
            }
        }
        return listS;
    }

    public static void main(String[] args) {
        ImgLista s = new ImgLista();
        for (int i = 0; i < s.buscarPorSemillero("Linux").size(); i++) {

            System.out.println(s.buscarPorSemillero("Linux").get(i).toString());
        }
//        System.out.println(s.buscarPorUser("1151408"));
    }

}
