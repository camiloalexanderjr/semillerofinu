/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

import java.util.List;
import Carrera.Carrera;
import Carrera.CarreraDao;
import java.util.ArrayList;

/**
 *
 * @author USUARIO
 */
public class CarreraLista {

    
    CarreraDao f = new CarreraDao();
    public Carrera buscar(String codigo) {
        Carrera n = null;

        List<Carrera> lista = f.llenarLista();
        for (Carrera sem : lista) {
           
            if (sem.getCodigo().equalsIgnoreCase(codigo)) {
                n = sem;
            }
        }
        return n;
    }
//
    
     public List buscarPorFacultad(String facultad) {
         List<Carrera> listS = new ArrayList<>();
       
        Carrera n = null;

        List<Carrera> lista = f.llenarLista();
        for (Carrera sem : lista) { 

            if (sem.getFacultad().equalsIgnoreCase(facultad)) {
                listS.add(sem);
            }
        }
        return listS;
    }
     
    public static void main(String[] args) {
      CarreraLista s =  new CarreraLista();
        System.out.println(s.buscarPorFacultad("Ingenieria").toString());
    }
   

}
