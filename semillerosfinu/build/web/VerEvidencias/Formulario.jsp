<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Controlador.RutaIMGDOCS"%>
<%@page import="Listas.FormularioLista"%>
<%@page import="Formulario.FormularioDao"%>
<%@page import="Formulario.Formulario"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="stylesheet" href="../assets/css/Img.css" />
        <style>
            td{
                height: 10px;
            }
        </style>
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Evidencias </strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <section id="search" class="alt">
                            <form method="post" action="#">
                                <input type="text" name="query" id="buscar" placeholder="Escriba elemento a filtrar" />
                            </form>
                    </section>
                    <br>





                    <br class="mt-4">
                    <br>
                    <div class="container mt-4 ">
                        <div class="form-group">

                        </div>
                    </div>



                    <div class="row " style="border: outset lightgrey 5px">
                        <br>

                        <table id="tabla">
                            <thead>
                                <tr>
                                    <th>Documento</th>
                                    <th>NombreDoc</th>
                                    <th>Observacion</th>
                                    <th>Revision</th>                                    
                                    <th>    </th>
                                    <th>Reclamo:</th>
                                </tr>
                            </thead>
                            <%
                                String r = "";
                                String aux = "";
                                FormularioLista a = new FormularioLista();
                                List<Formulario> lista = a.buscarPorSemillero(RutaIMGDOCS.semillero);
                                for (Formulario arts : lista) {
                                    if (arts.getObservacion() == null) {
                                        r = "";
                                    }
                            %>
                            <tbody>



                            <form action="../Evidencia?accion=Enviar" class="col-md-6" method="POST">
                                <tr>
                                    <td>
                                        <ul>
                                            <li style="list-style: none">
                                                <a style="color: white" href="<%=arts.getRuta()%>" download>
                                                    <img src="../images/pdf.png" width="100px" height="100px" style="padding-left: 10px; margin-top: 10px" >
                                                </a>

                                            </li>

                                        </ul>
                                    </td>
                                    <td>
                                        <p style="text-align: left; position: relative; top: -90px" id="nombre"> <%=arts.getNombre()%></p>
                                        <input hidden value="<%=arts.getNombre()%>" name="nombre">
                                    </td>
                                    <td>
                                        <textarea style="position: relative; top: -90px" name="observacion" rows="5" cols="60"><%=arts.getObservacion()%></textarea> 
                                    </td>
                                    <td>
                                        <%
                                            if (arts.getRevision() == 1) {
                                        %>
                                        <input type="checkbox" id="huey"  name="revisado" value="1" checked><label for="huey"></label>
                                        <%} else {%>
                                        <input type="checkbox" id="huey<%=arts.getCodigo()%>" onclick="cambiaestado(this)" name="revisado" value="0"><label for="huey<%=arts.getCodigo()%>"></label>


                                        <%}%>
                                    </td>
                                    <td>
                                        <button type="submit" class="button" style="position: relative; top: -90px">Enviar</button>
                                    </td>
                                     <td>
                                        <textarea style="position: relative; top: -90px" name="reclamo" rows="5" cols="60" disabled><%=arts.getReclamo() %></textarea> 
                                    </td>
                                </tr>
                            </form>
                            <%}%> 



                            </tbody>
                        </table>



                    </div>




                </div>
            </div>







                    <!-- Search -->
                    <div id="sidebar">
                        <div class="inner">

                            <!-- Search -->
                            <section id="search" class="alt">
                                <form method="post" action="#">
                                    <input type="text" name="query" id="query" placeholder="Search" />
                                </form>
                            </section>

                            <!-- Menu -->
                            <%@include file="menu.jsp" %>

                            <!-- Section -->


                            <!-- Footer -->
                            <%@include file="../footer.jsp" %>
                        </div>
                    </div>

              

        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>
        <script src="../assets/js/filtrar.js"></script>
        <script>
                                            function cambiaestado(e) {
                                                $(e).val(1);
                                                if ($(e).prop('checked')) {
                                                    $(e).val(1);
                                                }else{
                                                    $(e).val(0);
                                                }
                                            }
        </script>
    </body>
</html>
