<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Usuario.Usuario"%>
<%@page import="Controlador.Logins"%>
<%@page import="Listas.UsuarioLista"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="Usuario.UsuarioDao"%>
<%@page import="Listas.UsuarioLista"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Datos.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Información Personal</strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <br>
                    <%
                        UsuarioLista a = new UsuarioLista();

                        Usuario x = a.buscar(Logins.user + "");

                    %>
                    <div>
                        <center>
                            <img src=" <%=x.getFoto()%>" class="img logo rounded-circle mb-5" width="25%" alt="User Image" >

                        </center> 
                    </div>
                    <div>
                        <table border="1" cellspacing="1" cellpadding="1">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Correo</th>
                                    <th>Carrera</th>
                                    <th>Telefono</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr>
                                    <td><%=x.getCodigo()%></td>
                                    <td><%=x.getNombre()%></td>
                                    <td><%=x.getCorreo()%></td>
                                    <td><%=x.getCarrera()%></td>
                                    <td><%=x.getTelefono()%></td>
                                </tr>
                            </tbody>


                        </table>

                    </div>
                </div>
            </div>



            <!-- Search -->
            <div id="sidebar">
                <div class="inner">

                    <!-- Search -->
                    <section id="search" class="alt">
                        <%@include file="./Foto.jsp" %>
                    </section>

                    <!-- Menu -->
                    <%@include file="menu.jsp" %>

                    <!-- Section -->


                    <!-- Footer -->
                    <%@include file="../footer.jsp" %>
                </div>
            </div>



        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>

    </body>
</html>
