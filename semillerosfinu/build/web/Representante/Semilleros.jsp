<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Listas.UsuarioLista"%>
<%@page import="Controlador.Logins"%>
<%@page import="Listas.SemillerosLista"%>
<%@page import="Semillero.SemilleroDao"%>
<%@page import="java.util.List"%>
<%@page import="Semillero.Semillero"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Semilleros </strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <section id="search" class="alt">
                        <form method="post" action="#">
                            <input type="text" name="query" id="buscar" placeholder="Escriba elemento a filtrar" />
                        </form>
                    </section>
                    <br>
                    <form action="../RutaIMGDOCS" method="post">
                        <table border="1" id="tabla">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Representante</th>
                                    <th>Carrera</th>
                                    <th>Evidencias</th>
                                </tr>
                            </thead>

                            <%
                                UsuarioLista u = new UsuarioLista();
                                SemillerosLista a = new SemillerosLista();
                                List<Semillero> lista = a.buscarPorCarrera(u.buscar(Logins.user).getCarrera());
                                for (Semillero arts : lista) {

                            %>


                            <tbody>

                                <tr>
                                    <td><%=arts.getCodigo()%></td>
                                    <td ><%=arts.getNombre()%></td>
                                    <td><%=arts.getRepresentante()%></td>
                                    <td><%=arts.getCarrera()%></td>
                                    <td>
                                        <a href="../RutaIMGDOCS?accion=ImagenesDir&id=<%=arts.getNombre()%>">Imagenes</a>
                                        <br>
                                        <br>
                                        <a href="../RutaIMGDOCS?accion=DocumentosDir&id=<%=arts.getNombre()%>">Documentos</a>
                                    </td>
                                </tr>

                            </tbody>

                            <%}%>
                        </table>
                    </form>

                </div>
            </div>




            <!-- Search -->
            <div id="sidebar">
                <div class="inner">

                    <!-- Search -->
                    <section id="search" class="alt">
                        <%@include file="./Foto.jsp" %>
                    </section>

                    <!-- Menu -->
                    <%@include file="menu.jsp" %>

                    <!-- Section -->


                    <!-- Footer -->
                    <%@include file="../footer.jsp" %>
                </div>
            </div>



        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>
        <script src="../assets/js/filtrar.js"></script>
    </body>
</html>
