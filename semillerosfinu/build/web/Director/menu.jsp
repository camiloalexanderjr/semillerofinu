<%-- 
    Document   : menu
    Created on : 4/11/2021, 12:37:22 AM
    Author     : camil
--%>

<nav id="menu">
    <header class="major">
        <h2>Menu</h2>
    </header>
    <ul>

        <li><a href="VistaP.jsp">Inicio</a></li>
        <li>

            <span class="opener">Datos</span>
            <ul>
                <li>  <a href="Datos.jsp">Informacion</a></li>
                <li><a href="EditarDatos.jsp">Modificar Informacion</a></li>
            </ul>

        </li>

        <li>

            <span class="opener">Semilleros</span>
            <ul>
                <li>
                    <a href="Semilleros.jsp">Mostrar Semilleros </a>
                </li>
                <li>
                    <a href="Formatos.jsp">Formularios </a>
                </li>

            </ul>
        </li>
    </ul>
</nav>