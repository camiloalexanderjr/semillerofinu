<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Facultad.Facultad"%>
<%@page import="Facultad.FacultadDao"%>
<%@page import="Usuario.UsuarioDao"%>
<%@page import="Usuario.Usuario"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Registro de Carrera</strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <section id="banner">
                        <form action="../regFacultad?accion=Crear" method="post">

                            <label>Codigo</label>
                            <input  type="text" name="Codigo">
                            <label>Nombre</label>
                            <input  type="text" name="Nombre">

                            <label>Representante</label>
                            <select name="representante">
                                <option></option>
                                <%
                                    UsuarioDao r = new UsuarioDao();
                                    List<Usuario> rep = r.llenarLista();
                                    for (Usuario arts : rep) {
                                %>
                                <option ><%=arts.getNombre()%>   </option>>
                                <%}%>
                            </select>

                            <br>
                            <label>Facultad</label>
                            <select name="facultad">
                                <option></option>
                                <%
                                    FacultadDao f = new FacultadDao();
                                    List<Facultad> facu = f.llenarLista();
                                    for (Facultad fac : facu) {
                                %>
                                <option ><%=fac.getNombre()%>   </option>>
                                <%}%>
                            </select>

                            <br>
                            <input type="submit" value="Registrar">
                        </form>	

                    </section>

                </div>
            </div>



                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                      <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                        <!-- Menu -->
                       <%@include file="menu.jsp" %>
                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>

          

    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>

</body>
</html>
