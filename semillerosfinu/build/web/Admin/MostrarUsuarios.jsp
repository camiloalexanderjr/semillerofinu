<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Usuario.UsuarioDao"%>
<%@page import="Usuario.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="Listas.UsuarioLista"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Usuarios</strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <section id="search" class="alt">
                            <form method="post" action="#">
                                <input type="text" name="query" id="buscar" placeholder="Escriba elemento a filtrar" />
                            </form>
                    </section>
                    <br>

                    <table border="1" cellspacing="1" cellpadding="1" id="tabla">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Carrera</th>
                                <th>Telefono</th>
                                <th>Rol</th>
                            </tr>
                        </thead>

                        <%
                            UsuarioDao a = new UsuarioDao();
                            List<Usuario> lista = a.llenarLista();
                            for (Usuario arts : lista) {
                        %>
                        <tbody>
                            <tr>
                                <td><%=arts.getCodigo()%></td>
                                <td><%=arts.getNombre()%></td>
                                <td><%=arts.getCorreo()%></td>
                                <td><%=arts.getCarrera()%></td>
                                <td><%=arts.getTelefono()%></td>
                                <td><%=arts.getRol()%></td>
                            </tr>

                        </tbody>
                        <%}%>
                    </table>



                </div>
            </div>



                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                          <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                        <!-- Menu -->
                       <%@include file="menu.jsp" %>
                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>

          

    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>
    <script src="../assets/js/filtrar.js"></script>

</body>
</html>
