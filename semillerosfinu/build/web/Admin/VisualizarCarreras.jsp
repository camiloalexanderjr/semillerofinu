<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Controlador.Visualizar"%>
<%@page import="Carrera.Carrera"%>
<%@page import="Listas.CarreraLista"%>
<%@page import="Facultad.Facultad"%>
<%@page import="Facultad.FacultadDao"%>
<%@page import="Semillero.SemilleroDao"%>
<%@page import="java.util.List"%>
<%@page import="Semillero.Semillero"%>
<%@page import="Listas.SemillerosLista"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Bienvenido </strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <h1>Facultad de <%=Visualizar.Facultad%></h1>
                    <br>    
                    <form action="../Visualizar" method="post">
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Nombre</th>
                                <th>Representante</th>
                            </tr>
                        </thead>

                        <%
                            CarreraLista u = new CarreraLista();
                            List<Carrera> lista = u.buscarPorFacultad(Visualizar.Facultad);
                            for (Carrera arts : lista) {


                        %>


                        <tbody>
                            <tr>
                                <td>
                                    <p><%=arts.getCodigo()%></p>
                                </td>
                                   <td>
                                    <a href="../Visualizar?accion=Semillero&id=<%=arts.getNombre()%>"><%=arts.getNombre()%></a>
                                </td>
                                <td>
                                   <%=arts.getRepre()%>
                                </td>
                          
                             
                            </tr>
                        </tbody>
                        <%}%>
                    </table>
                    </form>

                </div>
                    <center><a href="VisualizarFacultad.jsp" class="button">Regresar</a></center>
            </div>


                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                        <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                         <%@include file="menu.jsp" %>
                        <!-- Section -->

                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>


    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>

</body>
</html>
