<%-- 
    Document   : RecoveryPass
    Created on : 4/06/2021, 11:37:13 PM
    Author     : USUARIO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" type="text/css" href="../assets/css/passCSS.css">
    </head>
    <body>

        <div class="container">



            <div class="cover">
                <h1>Correo Electronico.</h1>
                <form  class="flex-form" action="../RecuperarContrasena?accion=Enviar" method="POST">
                    <label for="from">
                        <i class="ion-location"></i>
                    </label>
                    <input type="mail" placeholder="@mail.com" name="correo">
                    <input type="submit" name="accion"  value="Enviar">
                </form>

            </div>

        </div>
    </body>
</html>
