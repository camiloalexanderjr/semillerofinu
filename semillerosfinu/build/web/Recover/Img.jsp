<%-- 
    Document   : Img
    Created on : 5/06/2021, 09:36:01 PM
    Author     : USUARIO
--%>

<%@page import="Img.Img"%>
<%@page import="java.util.List"%>
<%@page import="Img.ImgDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    </head>
    <body>
        <div class="row galeria">


            <%
                ImgDao a = new ImgDao();
                List<Img> lista = a.llenarLista();
                for (Img arts : lista) {
            %>
            <div class="col s12 m4 l3"> <div class="material-placeholder">

                    <img src=" <%=arts.getRuta()%>" alt="" class="responsive-img materialboxed">

                </div></div>

            <%}%> 

        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script src="../assets/js/mains.js"></script>
    </body>
</html>
