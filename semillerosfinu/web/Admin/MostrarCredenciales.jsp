<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Login.LoginDao"%>
<%@page import="java.util.List"%>
<%@page import="Listas.LoginLista"%>
<%@page import="Login.Login"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Usuarios</strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <section id="search" class="alt">
                            <form method="post" action="#">
                                <input type="text" name="query" id="buscar" placeholder="Escriba elemento a filtrar" />
                            </form>
                    </section>
                    <br>
                    <form action="../Login" method="post">
                        <table  class="table-wrapper"  class="title" id="tabla">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Cedula</th>
                                    <th>Estado</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <%
                                String e = "";
                                LoginDao a = new LoginDao();
                                List<Login> lista = a.llenarLista();
                                for (Login arts : lista) {
                                    if (arts.getEstado() == 1) {
                                        e = "Activo";
                                    }else{
                                        e="Inactivo";
                                    }
                            %>
                            <tbody>
                                <tr>
                                    <td><p><%=arts.getCodigo()%></p></td>
                                    <td><%=arts.getCedula()%></td>
                                    <td><%=e%></td>
                                    <td> <a  href="../CrearUsuario?accion=Hab&id=<%=arts.getCodigo()%>">Habilitar</a></td>

                                    <td><a  href="../CrearUsuario?accion=DesH&id=<%=arts.getCodigo()%>">Deshabilitar</a></td>
                                </tr>
                                <%}%>

                            </tbody>
                        </table>
                    </form>

                </div>
            </div>





                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                         <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                        <!-- Menu -->
                         <%@include file="menu.jsp" %>
                
                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>


    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>
    <script src="../assets/js/filtrar.js"></script>
</body>
</html>
