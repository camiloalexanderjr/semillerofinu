<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Controlador.DirectorServlet"%>
<%@page import="Semillero.Semillero"%>
<%@page import="Semillero.SemilleroDao"%>
<%@page import="Usuario.UsuarioDao"%>
<%@page import="Carrera.CarreraDao"%>
<%@page import="Usuario.Usuario"%>
<%@page import="Listas.UsuarioLista"%>
<%@page import="Carrera.Carrera"%>
<%@page import="java.util.List"%>
<%@page import="Listas.CarreraLista"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros | Editar Representante</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Editar Representante Semillero</strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <%
                        SemilleroDao sdao=new SemilleroDao();
                        Semillero sem=sdao.getId(DirectorServlet.semillero);
                       
                    %>
                    <section id="banner">
                        <form action="../DirectorServlet?action=updateRepre&id=<%=sem.getCodigo()%>" method="post"> 
                            <label>Codigo</label>
                            <input  type="text" name="codigosd" value="<%=sem.getCodigo()%>" disabled>
                            <label>Nombre Semillero </label>
                            <input  type="text" name="nombre" value="<%=sem.getNombre()%>" disabled>


                            <label>Representante</label>
                            <select name="representante">
                                <option></option>
                                <%
                                    UsuarioDao r = new UsuarioDao();
                                    List<Usuario> rep = r.llenarLista();
                                    for (Usuario arts : rep) {
                                    
                                %>
                                <% if(!arts.getNombre().equals(sem.getRepresentante())){%>
                                <option value="<%=arts.getCodigo()%>"><%=arts.getNombre()%></option>
                                <%}%>
                                <%}%>
                            </select>

                            <br>

                            <input type="submit" value="Cambiar Representante">
                        </form>

                    </section>

                </div>
            </div>




                    <!-- Search -->
                    <div id="sidebar">
                        <div class="inner">

                            <!-- Search -->
                              <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                            <!-- Menu -->
                            <%@include file="menu.jsp" %>
                            <!-- Section -->


                            <!-- Footer -->
                            <%@include file="../footer.jsp" %>
                        </div>
                    </div>

               
    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>

</body>
</html>
