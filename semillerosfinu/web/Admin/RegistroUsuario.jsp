<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Carrera.Carrera"%>
<%@page import="java.util.List"%>
<%@page import="Carrera.CarreraDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Registro de Usuarios</strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <section id="banner">
                        <form action="../regRepresentante?accion=crear" method="post">
                            <label>Codigo</label>
                            <input  type="text" name="codigo">
                            <label>Cedula</label>
                            <input  type="text" name="cedula">
                            <label>Nombre</label>
                            <input  type="text" name="nombre" >
                            <label>Mail</label>
                            <input  type="mail" placeholder="@gmail.com"name="correo" >
                            <label>Carrera</label>
                            <select name="carrera">
                                 <option></option>
                                <%
                                    CarreraDao c = new CarreraDao();
                                    List<Carrera> carr = c.llenarLista();
                                    for (Carrera car : carr) {
                                %>
                                <option ><%=car.getNombre()%>   </option>>
                                <%}%>
                            </select>
                            <label>Telefono</label>
                            <input  type="text" name="telefono">
                            <label>Rol</label>
                            <select name="rol">
                                <option></option>
                                <option>Cordinador (Admin)</option>
                                <option>Director</option>
                                <option>Representante</option>
                            </select>
                           
                            <br>


                            <input type="submit" name="accion" value="Registrar">
                        </form>	

                    </section>

                </div>
            </div>




                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                          <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                        <!-- Menu -->
                        <%@include file="menu.jsp" %>
                        <!-- Section -->

                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>

           

    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>

</body>
</html>
