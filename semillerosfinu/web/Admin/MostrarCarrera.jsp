<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Carrera.CarreraDao"%>
<%@page import="Carrera.Carrera"%>
<%@page import="java.util.List"%>
<%@page import="Listas.CarreraLista"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Carreras</strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <section id="search" class="alt">
                            <form method="post" action="#">
                                <input type="text" name="query" id="buscar" placeholder="Escriba elemento a filtrar" />
                            </form>
                    </section>
                    <br>
                    <form action="../regFacultad" method="post">
                        <table border="1" cellspacing="1" cellpadding="1" id="tabla">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Representante</th>
                                    <th>Facultad</th>
                                </tr>
                            </thead>
                            <%
                                String aux = "";
                                CarreraDao a = new CarreraDao();
                                List<Carrera> lista = a.llenarLista();
                                for (Carrera arts : lista) {
                                    if (arts.getRepre() == null) {
                                        aux = "Sin Asignar";
                                    } else {
                                        aux = arts.getRepre();
                                    }
                            %>
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="../regFacultad?accion=actualizar&id=<%=arts.getCodigo()%>"><%=arts.getCodigo()%></a>
                                    </td>
                                    <td><%=arts.getNombre()%></td>
                                    <td><%=aux%></td>
                                    <td><%=arts.getFacultad()%></td>
                                </tr>
                            </tbody>
                            <%}%>
                        </table>
                    </form>


                </div>
            </div>




                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                      <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                        <!-- Menu -->
                     <%@include file="menu.jsp" %>
                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>


    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>
    <script src="../assets/js/filtrar.js"></script>
</body>
</html>
