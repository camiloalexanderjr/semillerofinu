<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Usuario.Usuario"%>
<%@page import="Listas.UsuarioLista"%>
<%@page import="Controlador.Logins"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Bienvenido</strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <div>
                        <br>    

                        <form action="../EditarFoto?accion=editarAdmin" method="post" enctype="multipart/form-data">
                            <div>
                                <center> 
                                    <center> <input type="file"  name="fileImagen">   
                            <input type="submit" value="Editar IMG">
                                    </center>  

                            </div>
                        </form>
                        <form action="../regRepresentante?accion=editarAdmin" method="post" >
                            <%
                                UsuarioLista a = new UsuarioLista();

                                Usuario x = a.buscar(Logins.user + "");

                            %>
                            <div>
                                <center> 
                                    <img src=" <%=x.getFoto()%>"  width="25%"class="img logo rounded-circle mb-5" alt="User Image" ></center>
                        
                            </div>
                            <br>

                            <table border="1" cellspacing="1" cellpadding="1">
                                <tbody>


                                    <tr>
                                        <th>Codigo</th>
                                        <th><input type="text" name="codigo" value="<%=x.getCodigo()%>" readonly ></th>
                                    </tr>
                                    <tr>
                                        <th>Nombre</th>
                                        <th><input name="nombre" type="text" value="<%=x.getNombre()%>"></th>
                                    </tr>
                                    <tr>
                                        <th>Correo</th>
                                        <th><input name="correo" type="text" value="<%=x.getCorreo()%>"></th>
                                    </tr>
                                    <tr>
                                        <th>Carrera</th>
                                        <th><input type="text"  value="<%=x.getCarrera()%>" readonly></th>
                                    </tr>
                                    <tr>
                                        <th>Telefono</th>
                                        <th><input name="telefono" type="text" value="<%=x.getTelefono()%>"></th>
                                    </tr>
                                </tbody>


                            </table>

                            <input type="submit" value="Editar">
                        </form>
                    </div>
                </div>
            </div>


            <!-- Search -->
            <div id="sidebar">
                <div class="inner">

                    <!-- Search -->
                      <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>
                    <!-- Menu -->
                    <%@include file="menu.jsp" %>
                    <!-- Section -->


                    <!-- Footer -->
                    <%@include file="../footer.jsp" %>
                </div>
            </div>


        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>

    </body>
</html>
