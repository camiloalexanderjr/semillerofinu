<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Registro de Usuarios</strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <section id="banner">
                        <form action="../CrearUsuario?accion=Crear" method="post">
                            <label>Codigo</label>
                            <input  type="text" name="codigo">
                            <label>Cedula</label>
                            <input  type="text" name="cedula">
                            <label>Contraseña</label>
                            <input  type="text" name="contrasena">
                            <label>Estado</label>
                            <select name="estado">
                                <option></option>
                                <option>Activo</option>
                                <option>Inactivo</option>
                            </select>

                            <br>
                            <input type="submit" value="Registrar">
                        </form>	

                    </section>

                </div>
            </div>




                <!-- Search -->
                <div id="sidebar">
                    <div class="inner">

                        <!-- Search -->
                        <section id="search" class="alt">
                            <%@include file="./Foto.jsp" %>
                        </section>

                        <!-- Menu -->
                         <%@include file="menu.jsp" %>
                        <!-- Section -->


                        <!-- Footer -->
                        <%@include file="../footer.jsp" %>
                    </div>
                </div>

           
    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/util.js"></script>
    <script src="../assets/js/main.js"></script>

</body>
</html>
