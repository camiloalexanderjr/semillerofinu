<%-- 
    Document   : menu
    Created on : 4/11/2021, 12:08:14 AM
    Author     : camil
--%>

<nav id="menu">
    <header class="major">
        <h2>Menu</h2>
    </header>
    <ul>

        <li><a href="VistaP.jsp">Inicio</a></li>
        <li>

            <span class="opener">Datos</span>
            <ul>
                <li>  <a href="../Admin/Datos.jsp">Informacion</a></li>
                <li><a href="EditarDatos.jsp">Modificar Informacion</a></li>
            </ul>

        </li>
        <li>

            <span class="opener">Semilleros</span>
            <ul>
                <li>  <a href="RegistroSem.jsp">Registrar Semilleros</a></li>
                <li><a href="mostrarSemillero.jsp">Mostrar Semilleros</a></li>
            </ul>

        </li>

        <li>

            <span class="opener">Usuarios</span>
            <ul>
                <li>  <a href="RegistroUsuario.jsp">Registrar Usuario</a></li>
                <li><a href="MostrarUsuarios.jsp">Mostrar Usuarios</a></li>
            </ul>

        </li>
        <li>

            <span class="opener">Credenciales</span>
            <ul>
                <li>  <a href="RegistrarCredenciales.jsp">Registrar Credencial</a></li>
                <li><a href="MostrarCredenciales.jsp">Mostrar Credenciales</a></li>
            </ul>
        </li>
        <li>

            <span class="opener">Carrera</span>
            <ul>
                <li><a href="MostrarCarrera.jsp">Mostrar Carreras</a></li>
                <li><a href="RegistroCarrera.jsp">Registrar Carrera</a></li>
            </ul>
        </li>
        <li>
            <span class="opener">Visualizar</span>
            <ul>
                <li><a href="VisualizarFacultad.jsp">Miscelanea</a></li>
            </ul>
        </li>


    </ul>
</nav>