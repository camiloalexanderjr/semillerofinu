<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Controlador.RutaIMGDOCS"%>
<%@page import="Listas.ImgLista"%>
<%@page import="Img.Img"%>
<%@page import="Img.ImgDao"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="stylesheet" href="../assets/css/Img.css" />

    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Registro de Semilleros</strong>
                        <%@include file="../header.jsp" %>
                    </header>


                    <div class="container mt-4" >
                        <div class="form-group">
                          
                        </div>
                    </div>

                    <%--   <iframe style="border: none; overflow-x: hidden; overflow-y: hidden " width="100%"  height="500px" src="./Recover/Img.jsp">
                        
                    </iframe>           --%>        
                    <form action="../Evidencia?accion=EliminarImg" class="col-md-6" method="POST" enctype="multipart/form-data">

                        <div class="row galeria" style="border: outset lightgrey 5px">


                            <%
                                ImgLista a = new ImgLista();
                                List<Img> lista = a.buscarPorSemillero(RutaIMGDOCS.semillero + "");
                                for (Img arts : lista) {
                            %>
                            <div class="col s12 m4 l3"> 
                                <div class="material-placeholder">
                                    <a href="<%=arts.getRuta()%>" download>
                                    <img src=" <%=arts.getRuta()%>"  width="100px" height="100px" style="padding-left: 10px; margin-top: 10px" alt="" class="">
                                    </a>
                                    <div width="50px" >
                                        <p style="text-align: center; "> <%=arts.getObservacion()%></p>
                                    </div>
                                  
                                </div>
                            </div>

                            <%}%> 

                        </div>
                    </form>






                </div>
            </div>






                    <!-- Search -->
                    <div id="sidebar">
                        <div class="inner">

                            <!-- Search -->
                            <section id="search" class="alt">
                                <form method="post" action="#">
                                    <input type="text" name="query" id="query" placeholder="Search" />
                                </form>
                            </section>

                            <!-- Menu -->
                            <%@include file="menu.jsp" %>

                            <!-- Section -->


                            <!-- Footer -->
                            <%@include file="../footer.jsp" %>
                        </div>
                    </div>


        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>

    </body>
</html>
