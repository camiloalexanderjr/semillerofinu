<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Controlador.Logins"%>
<%@page import="Formulario.Formulario"%>
<%@page import="Listas.FormularioLista"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="Usuario.Usuario"%>
<%@page import="Listas.UsuarioLista"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="shortcut icon" href="images/icon.png"> 
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Director</strong>
                        <%@include file="../header.jsp" %>
                    </header>
                    <section id="search" class="alt">
                        <form method="post" action="#">
                            <input type="text" name="query" id="buscar" placeholder="Escriba elemento a filtrar" />
                        </form>
                    </section>

                    <h2>Formatos</h2>
                    <br>
                    <table id="tabla">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>NombreDoc</th>


                            </tr>
                        </thead>
                        <%

                            FormularioLista f = new FormularioLista();
                            List<Formulario> listas = f.buscarPorCarrera(Logins.carrera);
                            for (Formulario arts : listas) {

                        %>
                        <tbody>



                            <tr>
                                <td>
                                    <ul>
                                        <li style="list-style: none">
                                            <a style="color: white" href="<%=arts.getRuta()%>" download>
                                                <img src="../images/word.png" width="100px" height="100px" style="padding-left: 10px; margin-top: 10px" >
                                            </a>

                                        </li>

                                    </ul>
                                </td>
                                <td>
                                    <p style="text-align: left; position: relative; top: -90px" > <%=arts.getNombre()%></p>

                                </td>
                                
                            </tr>

                            <%}%> 



                        </tbody>
                    </table>


                </div>
            </div>



            <!-- Search -->
            <div id="sidebar">
                <div class="inner">

                    <!-- Search -->
                    <section id="search" class="alt">
                        <%@include file="./Foto.jsp" %>
                    </section>

                    <!-- Menu -->
                    <%@include file="menu.jsp" %>

                    <!-- Section -->


                    <!-- Footer -->
                    <%@include file="../footer.jsp" %>
                </div>
            </div>



        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>
        <script src="../assets/js/filtrar.js"></script>
    </body>
</html>
