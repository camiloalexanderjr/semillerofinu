<%-- 
    Document   : menu
    Created on : 4/11/2021, 10:20:44 AM
    Author     : camil
--%>

<nav id="menu">
    <header class="major">
        <h2>Menu</h2>
    </header>
    <ul>

        <li><a href="../Director/VistaP.jsp">Inicio</a></li>
        <li>

            <span class="opener">Datos</span>
            <ul>
                <li>  <a href="../Director/Datos.jsp">Informacion</a></li>
                <li><a href="../Director/EditarDatos.jsp">Modificar Informacion</a></li>
            </ul>

        </li>

        <li>

            <span class="opener">Semilleros</span>
            <ul>
                <li>
                    <a href="../Director/Semilleros.jsp">Mostrar Semilleros </a>
                </li>
                <li>
                    <a href="../Director/Formatos.jsp">Formularios </a>
                </li>

            </ul>
        </li>
    </ul>
</nav>
