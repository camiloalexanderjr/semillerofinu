<%-- 
    Document   : VistaP
    Created on : Apr 7, 2021, 9:00:13 PM
    Author     : USUARIO
--%>

<%@page import="Controlador.Logins"%>
<%@page import="Controlador.RutaIMGDOCS"%>
<%@page import="Listas.FormularioLista"%>
<%@page import="Formulario.FormularioDao"%>
<%@page import="Formulario.Formulario"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE HTML>

<html>
    <head>
        <title>Semilleros</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="../assets/css/Admin.css" />
        <link rel="stylesheet" href="../assets/css/Img.css" />
        <style>
            td{
                height: 10px;
            }
        </style>
    </head>
    <body class="is-preload">

        <%@include file="../aside.jsp"%>


        <!-- Wrapper -->
        <div id="wrapper">

            <!-- Main -->   
            <div id="main">
                <div class="inner">

                    <!-- Header -->
                    <header id="header">
                        <strong class="logo">Evidencias </strong>
                        <%@include file="../header.jsp" %>
                    </header>

                    <br>





                    <br class="mt-4">
                    <br>
                    <div class="container mt-4 ">
                        <div class="form-group">
                            <form action="../Evidencia?accion=DocumentosRepre" class="col-md-6" method="POST" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>Subir Formulario Diligenciado (Solo pdf)</h3>
                                    </div>
                                    <div class="card-body">
                                        <div >

                                            <br>
                                            <div class="form-group">
                                                <h4>Documento</h4>
                                                <input type="file" name="fileImagen">
                                            </div>

                                        </div>
                                        <br>
                                        <div class="card-footer">
                                            <button class="btn btn-outline-primary" name="accion" value="Documentos">Guardar Documentos</button>
                                        </div>                
                                    </div> 


                                </div>

                            </form>
                        </div>
                    </div>



                    <div class="row " style="border: outset lightgrey 5px">
                        <br>
                        <form action="../Evidencia?accion=EliminarDoc" class="col-md-6" method="POST" enctype="multipart/form-data">

                            <table >
                                <thead>
                                    <tr>
                                        <th>Documento</th>
                                        <th>NombreDoc</th>

                                    </tr>
                                </thead>
                                <%

                                    FormularioLista a = new FormularioLista();
                                    List<Formulario> lista = a.buscarPorCarrera(Logins.carrera);
                                    for (Formulario arts : lista) {

                                %>
                                <tbody>



                                    <tr>
                                        <td>
                                            <ul>
                                                <li style="list-style: none">
                                                    <a style="color: white" href="<%=arts.getRuta()%>" download>
                                                        <img src="../images/word.png" width="100px" height="100px" style="padding-left: 10px; margin-top: 10px" >
                                                    </a>

                                                </li>
                                                <li style="list-style: none;" >
                                                    <a href="../Evidencia?accion=RepreEliminarDoc&id=<%=arts.getNombre()%>">Eliminar</a>

                                                </li>

                                            </ul>
                                        </td>
                                        <td>
                                            <p style="text-align: left; position: relative; top: -90px" > <%=arts.getNombre()%></p>



                                    </tr>

                                    <%}%> 



                                </tbody>
                            </table>
                        </form>


                    </div>




                </div>
            </div>







            <!-- Search -->
            <div id="sidebar">
                <div class="inner">

                    <!-- Search -->
                    <section id="search" class="alt">
                        <%@include file="./Foto.jsp" %>
                    </section>

                    <!-- Menu -->
                    <%@include file="menu.jsp" %>
                    <!-- Section -->


                    <!-- Footer -->
                    <%@include file="../footer.jsp" %>
                </div>
            </div>



        </div>

        <!-- Scripts -->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/util.js"></script>
        <script src="../assets/js/main.js"></script>

    </body>
</html>
