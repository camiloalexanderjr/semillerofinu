<%-- 
    Document   : menu
    Created on : 4/11/2021, 12:59:55 AM
    Author     : camil
--%>
<nav id="menu">
    <header class="major">
        <h2>Menu</h2>
    </header>
    <ul>

        <li>
            <a href="VistaP.jsp">Inicio</a>
        </li>
        <li>

            <span class="opener">Datos</span>
            <ul>
                <li>  <a href="Datos.jsp">Informacion</a></li>
                <li><a href="editar.jsp">Modificar Informacion</a></li>
            </ul>

        </li>

        <li>

            <span class="opener">Semillero</span>
            <ul>
                <li> 
                    <a href="Semilleros.jsp">Semilleros </a>
                </li>
            </ul>
        </li>
        <li>

            <span class="opener">Formatos</span>
            <ul>
                <li> 
                    <a href="Documentos.jsp">Documentos </a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
